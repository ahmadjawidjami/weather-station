public class ForcastDisplay implements Observer, DisplayElement {

	private WeatherData wheaterData;

	private String forcastMessage;

	public ForcastDisplay(WeatherData weatherData) {
		this.setWheaterData(weatherData);
		weatherData.registerObserver(this);
	}

	public void display() {

		WeatherStation.frame.lblForcast.setText(this.forcastMessage);

	}

	public void update(double temperature, double humidity, double pressure) {

		if (temperature <= 0) {
			this.forcastMessage = "snowy weather";
		} else if (temperature > 0 && temperature <= 15) {
			if (temperature > 0 && temperature <= 5)
				this.forcastMessage = "rainy weather";
			else if (temperature > 5 && temperature <= 10)
				this.forcastMessage = "partial rain";
			else
				this.forcastMessage = "cloudy";

		} else if (temperature > 15 && temperature <= 30) {
			if (temperature > 15 && temperature <= 20)
				this.forcastMessage = "half cloudy";
			else if (temperature > 20 && temperature <= 25)
				this.forcastMessage = "half sunny";
			else

				this.forcastMessage = "clear sky";

		} else if (temperature > 30 && temperature <= 45) {
			if (temperature > 30 && temperature <= 35)
				this.forcastMessage = "sunny";
			else if (temperature > 35 && temperature <= 40)
				this.forcastMessage = "sunny and really warm";
			else
				this.forcastMessage = "getting hot";

		} else {
			this.forcastMessage = "really hot";
		}
		display();

	}

	public WeatherData getWheaterData() {
		return wheaterData;
	}

	public void setWheaterData(WeatherData wheaterData) {
		this.wheaterData = wheaterData;
	}

}
