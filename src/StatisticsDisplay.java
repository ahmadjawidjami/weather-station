public class StatisticsDisplay implements Observer, DisplayElement {

	private Subject weatherData;

	private double temperature;

	private double maxTemperature;
	private double minTemperature = 10000000;
	private double averageTemperature;

	public StatisticsDisplay(Subject weatherData) {

		this.setWeatherData(weatherData);

		weatherData.registerObserver(this);

	}

	public void display() {

		WeatherStation.frame.lblMaxTemperature.setText("Max Temperature: "
				+ this.maxTemperature + " degrees");
		WeatherStation.frame.lblMinTemperature.setText("Min temperature: "
				+ this.minTemperature + " degrees");
		WeatherStation.frame.lblAvgTemperature.setText("Avg temperature: "
				+ this.averageTemperature + " degrees");

	}

	public void update(double temperature, double humidity, double pressure) {

		if (maxTemperature < temperature) {

			maxTemperature = temperature;

		}

		if (minTemperature > temperature) {
			minTemperature = temperature;
		}

		averageTemperature = (maxTemperature + minTemperature) / 2;

		display();

	}

	public double getTemerature() {
		return temperature;
	}

	public void setTemerature(double temerature) {
		this.temperature = temerature;
	}

	public Subject getWeatherData() {
		return weatherData;
	}

	public void setWeatherData(Subject weatherData) {
		this.weatherData = weatherData;
	}

}
