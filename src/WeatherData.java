import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject {

	private double teperature;
	private double humidity;
	private double pressure;

	List<Observer> observers;

	public WeatherData() {
		this.observers = new ArrayList<Observer>();
	}

	@Override
	public void registerObserver(Observer observer) {

		observers.add(observer);

	}

	@Override
	public void removeObserver(Observer observer) {
		this.observers.remove(observers.indexOf(observer));

	}

	@Override
	public void notifyObservers() {

		for (Observer currentObserver : observers) {

			currentObserver.update(teperature, humidity, pressure);

		}

	}

	public void measurementsChanged() {

		this.notifyObservers();

	}

	public double getTeperature() {
		return teperature;
	}

	public void setTeperature(double teperature) {
		this.teperature = teperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

}
