public class CurrentConditions implements Observer, DisplayElement {

	private Subject weatherData;
	private double temerature;
	private double humidity;
	private double pressure;
	
	
	
	public CurrentConditions(Subject weatherData){
		this.setWeatherData(weatherData);
		weatherData.registerObserver(this);
		
	}
	@Override
	public void display() {
		
		
		WeatherStation.frame.lblTemperaturelabel.setText("temperature: "+this.temerature+" degree of celcius");
		WeatherStation.frame.lblHumiditylabel.setText("Humidity: "+this.humidity+" %");
		WeatherStation.frame.lblPressurelabel.setText("Pressure: "+ this.pressure+" inHg");
		

	}

	@Override
	public void update(double temperature, double humidity, double pressure) {
		this.temerature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		display();

	}
	
	public Subject getWeatherData() {
		return weatherData;
	}
	public void setWeatherData(Subject weatherData) {
		this.weatherData = weatherData;
	}

}
