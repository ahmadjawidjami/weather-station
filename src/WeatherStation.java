import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WeatherStation extends JFrame {
	private JTextField temperatureTextField;

	public static WeatherStation frame;

	static WeatherData weatherData;
	JLabel lblTemperaturelabel;
	JLabel lblHumiditylabel;
	JLabel lblPressurelabel;
	private JTextField humidityTextField;
	private JTextField pressureTextField;
	private JLabel lblTemperature;
	private JLabel lblHumidity;
	private JLabel lblPressure;
	private JLabel lblTemperature_1;
	private JLabel lblHumidity_1;
	private JLabel lblPressure_1;
	JLabel lblMaxTemperature;
	JLabel lblMinTemperature;
	JLabel lblAvgTemperature;
	JLabel lblForcast;
	private JLabel lblCurrentConditionsDisplay;
	private JLabel lblStattisticsDisplay;
	private JLabel lblForcastDisplay;

	public WeatherStation() {
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);

		lblTemperaturelabel = new JLabel("Temperature:");

		lblHumiditylabel = new JLabel("Humidity:");

		lblPressurelabel = new JLabel("Pressure:");

		temperatureTextField = new JTextField();
		temperatureTextField.setColumns(10);

		JButton btnSubmit = new JButton("submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					weatherData.setTeperature(Double
							.parseDouble(temperatureTextField.getText()));

					weatherData.setHumidity(Double
							.parseDouble(humidityTextField.getText()));
					weatherData.setPressure(Double
							.parseDouble(pressureTextField.getText()));
					weatherData.measurementsChanged();

				} catch (NumberFormatException exception) {

				}

			}
		});

		humidityTextField = new JTextField();
		humidityTextField.setColumns(10);

		pressureTextField = new JTextField();
		pressureTextField.setColumns(10);

		lblTemperature = new JLabel();

		lblHumidity = new JLabel();

		lblPressure = new JLabel();

		lblTemperature_1 = new JLabel("Temperature");

		lblHumidity_1 = new JLabel("Humidity");

		lblPressure_1 = new JLabel("Pressure");

		lblMaxTemperature = new JLabel("Max Temperature:");

		lblMinTemperature = new JLabel("Min Temperature:");

		lblAvgTemperature = new JLabel("Avg Temperature:");

		lblForcast = new JLabel("");

		lblCurrentConditionsDisplay = new JLabel("Current Conditions Display");

		lblStattisticsDisplay = new JLabel("Stattistics Display");

		lblForcastDisplay = new JLabel("Forcast Display");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGap(171)
																.addComponent(
																		btnSubmit))
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGap(36)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addComponent(
																						lblTemperature_1)
																				.addComponent(
																						lblTemperature))))
								.addContainerGap(550, Short.MAX_VALUE))
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGap(25)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addComponent(
																		lblCurrentConditionsDisplay)
																.addContainerGap())
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addComponent(
																						lblPressurelabel)
																				.addGroup(
																						gl_panel.createParallelGroup(
																								Alignment.TRAILING)
																								.addGroup(
																										gl_panel.createSequentialGroup()
																												.addComponent(
																														temperatureTextField,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE)
																												.addGap(18)
																												.addComponent(
																														humidityTextField,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE))
																								.addGroup(
																										gl_panel.createSequentialGroup()
																												.addGroup(
																														gl_panel.createParallelGroup(
																																Alignment.TRAILING)
																																.addComponent(
																																		lblHumidity_1)
																																.addComponent(
																																		lblHumidity))
																												.addGap(22))
																								.addComponent(
																										lblTemperaturelabel,
																										Alignment.LEADING)
																								.addComponent(
																										lblHumiditylabel,
																										Alignment.LEADING)))
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addPreferredGap(
																										ComponentPlacement.RELATED)
																								.addGroup(
																										gl_panel.createParallelGroup(
																												Alignment.TRAILING)
																												.addGroup(
																														gl_panel.createParallelGroup(
																																Alignment.LEADING)
																																.addComponent(
																																		lblAvgTemperature)
																																.addComponent(
																																		lblMinTemperature))
																												.addGroup(
																														gl_panel.createParallelGroup(
																																Alignment.LEADING)
																																.addComponent(
																																		lblStattisticsDisplay)
																																.addComponent(
																																		lblMaxTemperature)))
																								.addGap(74)
																								.addGroup(
																										gl_panel.createParallelGroup(
																												Alignment.LEADING)
																												.addComponent(
																														lblForcastDisplay)
																												.addComponent(
																														lblForcast))
																								.addGap(329))
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addGroup(
																										gl_panel.createParallelGroup(
																												Alignment.LEADING)
																												.addGroup(
																														gl_panel.createSequentialGroup()
																																.addGap(18)
																																.addComponent(
																																		pressureTextField,
																																		GroupLayout.PREFERRED_SIZE,
																																		GroupLayout.DEFAULT_SIZE,
																																		GroupLayout.PREFERRED_SIZE))
																												.addGroup(
																														gl_panel.createSequentialGroup()
																																.addGap(38)
																																.addGroup(
																																		gl_panel.createParallelGroup(
																																				Alignment.LEADING)
																																				.addComponent(
																																						lblPressure_1)
																																				.addComponent(
																																						lblPressure))))
																								.addContainerGap(
																										465,
																										Short.MAX_VALUE)))))));
		gl_panel.setVerticalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGap(5)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														lblCurrentConditionsDisplay)
												.addComponent(
														lblStattisticsDisplay)
												.addComponent(lblForcastDisplay))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														lblTemperaturelabel)
												.addComponent(lblMaxTemperature)
												.addComponent(lblForcast))
								.addGap(27)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(lblHumiditylabel)
												.addComponent(lblMinTemperature))
								.addGap(26)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(lblPressurelabel)
												.addComponent(lblAvgTemperature))
								.addPreferredGap(ComponentPlacement.RELATED,
										90, Short.MAX_VALUE)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.TRAILING)
												.addGroup(
														gl_panel.createParallelGroup(
																Alignment.BASELINE)
																.addComponent(
																		lblTemperature)
																.addComponent(
																		lblPressure)
																.addComponent(
																		lblHumidity))
												.addGroup(
														gl_panel.createParallelGroup(
																Alignment.BASELINE)
																.addComponent(
																		lblTemperature_1)
																.addComponent(
																		lblHumidity_1)
																.addComponent(
																		lblPressure_1)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														temperatureTextField,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														humidityTextField,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														pressureTextField,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
								.addGap(18).addComponent(btnSubmit).addGap(51)));
		panel.setLayout(gl_panel);
	}

	public static void main(String[] args) {

		frame = new WeatherStation();

		weatherData = new WeatherData();

		CurrentConditions currentDisplay = new CurrentConditions(weatherData);
		StatisticsDisplay statistics = new StatisticsDisplay(weatherData);
		ForcastDisplay forcast = new ForcastDisplay(weatherData);

		
	}
}
